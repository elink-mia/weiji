$(function() {
    var autoheight = function() {
        var w_h = $(window).height(),
            need_h = w_h - 30;
        $('.js-fullheight').height(need_h);
        $(window).resize(function() {
            $('.js-fullheight').height(need_h);
        });
    };
    autoheight();
    $("img.lazyload").lazyload();
    $('.sli-bg').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 1000,
        fade: true,
        pauseOnFocus: false,
        cssEase: 'linear'
    });
    $('#main .ellipsis').ellipsis({
        row: 2
    });
});