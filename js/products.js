$(function () {
    var dec_height = function (first) {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').height(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $(window).on('load', function () {
        footer_mb();
    });
    $(window).resize(function () {
        footer_mb();
        dec_height();
    });
    $("img.lazyload").lazyload();
    var filterSingle = $('.filtr-container').filterizr({
        layout: 'sameWidth'
    });
    $('.grid-filter li').click(function () {
        $('.grid-filter li').removeClass('active');
        $(this).addClass('active');
        var filter = $(this).data('filter');
        filterSingle.filterizr('filter', filter);
    });
    $('.quantity').each(function () {
        var index = $(this).children('input').val() != '' ? $(this).children('input').val() : 0;
        $(this).children('input').val(index);
        $(this).children('.add').on('click', function () {
            index++;
            $(this).siblings('input').val(index);
        });
        $(this).children('.sub').on('click', function () {
            index--;
            $(this).siblings('input').val(index);
        });
    });
    shadow();
    $('.mod-pro').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: true,
        infinite: true,
        speed: 1000,
        fade: true,
        cssEase: 'linear'
    });
    //mob版 - 商品搜尋
    $("#mob-buy .btn").click(function (e) {
        e.preventDefault();
        $("#buy-select, #page").addClass("active");
        $('.overlay').fadeIn();
        $('body').addClass("no-scroll");
    });
    $("#buy-close, .overlay").click(function () {
        $("#buy-select, #page").removeClass("active");
        $('.overlay').fadeOut();
        $('body').removeClass("no-scroll");
    });
});

var footer_mb = function () {
    var mob_buyH = $('#mob-buy').innerHeight(),
        winW = $(window).width();
    if (winW < 992) {
        $('#main').css('margin-bottom', mob_buyH);
    }
};

var shadow = function () {
    var width = 0;
    $('.grid-filter li').each(function() {
        width += parseInt($(this).outerWidth(), 0);
    });
    var title = $('.grid-filter .title').width(),
        totalWidth = width + title,
        win = $(window).width();
    if (totalWidth > win) {
        $('.grid-filter').addClass('shadowww');
    } else {
        $('.grid-filter').removeClass('shadowww');
    }
    console.log(width, title, totalWidth);
};