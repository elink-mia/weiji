$(function() {
    var autoheight = function() {
        var w_h = $(window).height(),
            need_h = w_h - 15;
        $('.js-fullheight').height(need_h);
        $(window).resize(function() {
            $('.js-fullheight').height(need_h);
        });
    };
    autoheight();
    $('.sli-banner').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 1000,
        fade: true,
        pauseOnFocus: false,
        cssEase: 'linear'
    });
    $('.sli-pro').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        centerMode: true,
        centerPadding: '15px',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              autoplay: true,
              autoplaySpeed: 2000,
              slidesToShow: 3,
              slidesToScroll: 1,
              speed: 1000,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              autoplay: true,
              autoplaySpeed: 2000,
              slidesToShow: 1,
              slidesToScroll: 1,
              speed: 1000,
              fade: true,
              infinite: true
            }
          }
        ]
      });
    $('#main .ellipsis').ellipsis({
        row: 2
    });
});