(function($) {
    var dec_height = function() {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').innerHeight(),
            need_h = win_h - content;
        if (content < win_h) {
            $('#main').css("min-height", main_h + need_h);
        }
        $(window).resize(function() {
            if (content < win_h) {
                $('#main').css("min-height", main_h + need_h);
            }
        });
    };
    $(window).on('load', function() {
        dec_height();
    });
    var pt_h = function() {
        var header = $('#header').height();
        $("#main.pdt").css('padding-top',header);
        $(window).resize(function() {
            $("#main.pdt").css('padding-top',header);
        });
    };
    var menu_mt = function() {
        var headerH = $('#header').innerHeight(),
            winW = $(window).width();
        if (winW < 992) {
            $('.offcanvas-collapse').css('bottom', '40px');
        }
    };
    var burgerMenu = function() {
        $('.menu').click(function() {
            $('.offcanvas-collapse').toggleClass('open');
            if ($('.offcanvas-collapse').hasClass('open')) {
                $('.burger, #header .logo').addClass('active');
                $('#header').addClass('fixed');
                $('body').addClass("no-scroll");
                $('.SideBar').on('click', function() {
                    // open sidebar
                    $('.offcanvas-collapse').removeClass('open');
                    $('.burger').removeClass('active');
                    $('#header').removeClass('fixed');
                    // fade in the overlay
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            } else {
                $('.burger').removeClass('active');
                $('#header').removeClass('fixed');
                $('body').removeClass("no-scroll");
                $('.sub-menu').removeClass('active');
            }
        });
    };
    var sidebar = function() {
        // when opening the sidebar
        $('.SideBar').on('click', function() {
            menu_mt();
            // open sidebar
            $('#sidebar').addClass('active');
            $('body').addClass("no-scroll");
            // fade in the overlay
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });

        // if dismiss or overlay was clicked
        $('#dismiss, .overlay').on('click', function() {
            // hide the sidebar
            $('#sidebar').removeClass('active');
            // fade out the overlay
            $('.overlay').fadeOut();
            $('body').removeClass("no-scroll");
        });
    };
    var windowScroll = function() {
        var winScroll = $(window).scrollTop() >= 1,
            pageH = $('#page').height(),
            winH = $(window).height(),
            headerH = $('#header').height(),
            x = pageH - winH;
        $(window).scroll(function() {
            if ($(window).scrollTop() > headerH && x >= headerH) {
                $('#header').addClass('fixed');
                menu_mt();
            } else {
                $('#header').removeClass('fixed');
                menu_mt();
            }
        });
    };
    var mobMenu = function() {
        $(".sub-menu-parent .icon").click(function() {
            if ($(this).siblings('.sub-menu').hasClass('active')) {
                $(this).siblings('.sub-menu').removeClass('active');
            } else {
                $('.sub-menu.active').removeClass('active');
                $(this).siblings('.sub-menu').addClass('active');
            }
        });
    };
    var goToTop = function() {
        $('.js-gotop').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');
            return false;
            menu_mt();
        });
        $(window).scroll(function() {
            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }
        });
    };
    $(function() {
        pt_h();
        menu_mt();
        burgerMenu();
        sidebar();
        windowScroll();
        mobMenu();
        goToTop();
    });
})(jQuery)